import {Component} from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';

/**
 * Generated class for the QrButtonComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
    selector: 'qr-button',
    templateUrl: 'qr-button.html'
})
export class QrButtonComponent {

    text: string;
    qrcode: string = '';

    constructor(private barcodeScanner: BarcodeScanner) {
        this.text = 'Scan QR Code';
    }

    scanQRCode() {
        this.barcodeScanner.scan({preferFrontCamera: false, showTorchButton: true, formats: 'QR_CODE'}).then((result) => {
            if (!result.cancelled && result.format === 'QR_CODE' && result.text) {
                this.qrcode = result.text;
            } else {
                this.qrcode = 'cancelled';
            }
        }, (err) => {
            this.qrcode = err;
        });
    }
}
