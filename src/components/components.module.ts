import { NgModule } from '@angular/core';
import { QrButtonComponent } from './qr-button/qr-button';
@NgModule({
	declarations: [QrButtonComponent],
	imports: [],
	exports: [QrButtonComponent]
})
export class ComponentsModule {}
